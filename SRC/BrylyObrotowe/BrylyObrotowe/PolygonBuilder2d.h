#pragma once

#include <vector>
#include "Vector.h"
#include "Polygon2d.h"
namespace RevolutionSolids
{

	class PolygonBuilder2d
	{
	public:
		PolygonBuilder2d() : _lines(std::vector<Line>()), _last(Vector(-1, -1, -1)){};
		void AddPoint(double x, double y);
		Polygon2d Build2dPolygon();
	private:
		std::vector<Line> _lines;
		Vector _last;
	};

}