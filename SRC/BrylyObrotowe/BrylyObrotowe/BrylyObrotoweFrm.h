///-----------------------------------------------------------------
///
/// @file      BrylyObrotoweFrm.h
/// @author    Wiktor
/// Created:   2015-01-29 13:28:14
/// @section   DESCRIPTION
///            BrylyObrotoweFrm class declaration
///
///------------------------------------------------------------------

#ifndef __BRYLYOBROTOWEFRM_H__
#define __BRYLYOBROTOWEFRM_H__

#ifdef __BORLANDC__
	#pragma hdrstop
#endif

#ifndef WX_PRECOMP
	#include <wx/wx.h>
	#include <wx/frame.h>
#else
	#include <wx/wxprec.h>
#endif

//Do not add custom headers between 
//Header Include Start and Header Include End.
//wxDev-C++ designer will remove them. Add custom headers after the block.
////Header Include Start
#include <wx/button.h>
#include <wx/scrolbar.h>
#include <wx/panel.h>
////Header Include End
#include "PolygonBuilder2d.h"
#include "ModelFacade.h"
////Dialog Style Start
#undef BrylyObrotoweFrm_STYLE
#define BrylyObrotoweFrm_STYLE wxCAPTION | wxSYSTEM_MENU | wxMINIMIZE_BOX | wxCLOSE_BOX
////Dialog Style End

class BrylyObrotoweFrm : public wxFrame
{
	private:
		DECLARE_EVENT_TABLE();
		
	public:
		BrylyObrotoweFrm(wxWindow *parent, wxWindowID id = 1, const wxString &title = wxT("BrylyObrotowe"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize(1000,768), long style = BrylyObrotoweFrm_STYLE);
		virtual ~BrylyObrotoweFrm();
		void WxPanel1UpdateUI(wxUpdateUIEvent& event);
		void WxPanel2UpdateUI(wxUpdateUIEvent& event);
		void WxScrollBar1Scroll(wxScrollEvent& event);
		void WxScrollBar2Scroll(wxScrollEvent& event);
		void WxScrollBar3Scroll(wxScrollEvent& event);
		void WxButton1Click(wxCommandEvent& event);
		void WxButton2Click(wxCommandEvent& event);
		void WxButton3Click(wxCommandEvent& event);
		void WxButton4Click(wxCommandEvent& event);
		void BrylyObrotoweFrmLeftDown0(wxMouseEvent& event);
		void WxCheckBox1Click(wxCommandEvent& event);
	private:
		//Do not add custom control declarations between
		//GUI Control Declaration Start and GUI Control Declaration End.
		//wxDev-C++ will remove them. Add custom code after the block.
		////GUI Control Declaration Start
		wxCheckBox *WxCheckBox1;
		wxButton *WxButton4;
		wxButton *WxButton3;
		wxButton *WxButton2;
		wxButton *WxButton1;
		wxScrollBar *WxScrollBar3;
		wxScrollBar *WxScrollBar1;
        wxScrollBar *WxScrollBar2;
		wxPanel *WxPanel2;
		wxPanel *WxPanel1;
		wxStaticText *WxST_X;
		wxStaticText *WxST_Y;
		wxStaticText *WxST_Z;
		wxStaticText *WxST_leftPanel;
		wxStaticText *WxST_rightPanel;
		wxStaticText *WxST_coordinatesPanel;
		wxSpinCtrl *WxSpinCtrl2;
		wxSpinCtrl *WxSpinCtrl1;
		////GUI Control Declaration End
		RevolutionSolids::ModelFacade _model;
		RevolutionSolids::PolygonBuilder2d _builder;
		RevolutionSolids::Polygon2d _polygon2d;
		RevolutionSolids::Polygon3d _polygon3d;
	private:
		//Note: if you receive any error with these enum IDs, then you need to
		//change your old form code that are based on the #define control IDs.
		//#defines may replace a numeric value for the enum names.
		//Try copy and pasting the below block in your old form header files.
		enum
		{
			////GUI Enum Control ID Start
			ID_WXST_COORDINATESPANEL = 1016,
			ID_WXST_LEFTPANEL = 1015,
			ID_WXST_RIGHTPANEL = 1014,
			ID_WXST_X = 1013,
			ID_WXST_Y = 1012,
			ID_WXST_Z = 1011,
            ID_WXBUTTON4 = 1010,
			ID_WXBUTTON3 = 1009,
			ID_WXBUTTON2 = 1008,
			ID_WXBUTTON1 = 1007,
			ID_WXSCROLLBAR3 = 1006,
			ID_WXSCROLLBAR2 = 1005,
			ID_WXSCROLLBAR1 = 1004,
			ID_WXPANEL2 = 1003,
			ID_WXPANEL1 = 1002,
			ID_WXSPINCTRL1 = 1017,
			ID_WXSPINCTRL2 = 1018,
			ID_WXCHECKBOX1 = 1019,
			ID_DUMMY_VALUE_ //don't remove this value unless you have other enum values
		};
		
	private:
		void OnClose(wxCloseEvent& event);
		void CreateGUIControls();		
		void RefreshAndDraw2d();
		void RefreshAndDraw3d();
};

#endif
