#pragma once

#include <vector>
#include "Rectangle.h"

namespace RevolutionSolids
{
	class Polygon3d
	{
	public:
		Polygon3d() :_figure(std::vector<Rectangle>()), _middleY(0), _middleZ(0) {};
		Polygon3d(std::vector <Rectangle>& vector, double middleY, double middleZ) : _figure(vector),
			_middleY(middleY), _middleZ(middleZ){};
		friend Polygon3d operator*(const Matrix& matrix, const Polygon3d& polygon);
		void Draw(wxDC& dc, bool transparency=true);
		double GetMiddleY() const { return _middleY; }
		double GetMiddleX() const { return _middleZ; }
	private:
		std::vector<Rectangle> _figure;
		double _middleY;
		double _middleZ;
	};
}