#pragma once

#include "Matrix.h"

#include <cmath>

namespace RevolutionSolids
{
	class MatrixRepository
	{
	public:
		static Matrix GetIdentityMatrix();
		
		static Matrix GetRotationMatrixOY(double phi);
		static Matrix GetRotationMatrixOX(double phi);
		static Matrix GetRotationMatrixOZ(double phi);

		static Matrix GetScaleMatrix(double sx, double sy, double sz);

		static Matrix GetTranslationMatrix(double tx, double ty, double tz);

		static Matrix GetPerspectiveMatrix(double r,
			double l, double t, double b, double n, double f);
	};
}