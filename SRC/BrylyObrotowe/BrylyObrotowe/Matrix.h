#ifndef MATRIX_H
#define MATRIX_H



#include "Vector.h"

	class Matrix
	{
	public:
		double Data[4][4];
		Matrix();
		Matrix operator*(const Matrix&);
		friend Vector operator*(const Matrix&, const Vector&);
	};

#endif