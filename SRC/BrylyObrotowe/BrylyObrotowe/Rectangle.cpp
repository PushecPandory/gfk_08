
#include "stdafx.h"

#include "Rectangle.h"


#include "NotImplementedException.h"

namespace RevolutionSolids
{
	void Rectangle::Draw(wxDC& context, bool transparency)
	{
	
		
		wxPoint polygon[4] = 
		{
			wxPoint(_points[0].X(), this->_points[0].Y()),
			wxPoint(_points[1].X(), this->_points[1].Y()),
			wxPoint(_points[2].X(), this->_points[2].Y()),
			wxPoint(_points[3].X(), this->_points[3].Y())
		};
		if (transparency)
		{
			context.DrawLine(polygon[0], polygon[1]);
			context.DrawLine(polygon[1], polygon[2]);
			context.DrawLine(polygon[2], polygon[3]);
			context.DrawLine(polygon[3], polygon[0]);
		}
		else
		{
			context.SetBrush(wxColor(_fillColor));
			context.SetPen(wxColor(_outlineColor));
			context.DrawPolygon(4, polygon);
		}
	}

	Rectangle operator*(const Matrix& matrix, const Rectangle& rect)
	{
		return Rectangle(matrix*rect._points[0], matrix*rect._points[1], matrix*rect._points[2], matrix*rect._points[3]);
	}
}