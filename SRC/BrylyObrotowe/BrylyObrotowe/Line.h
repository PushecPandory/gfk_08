#pragma once

#include "Vector.h"
#include "Matrix.h"
namespace RevolutionSolids{
	class Line
	{
	public:
		Line(const Vector& begin, const Vector& end) : Begin(begin), End(end) {};
		Vector Begin, End;
		friend Line operator*(const Matrix& matrix, const Line line)
		{
			return Line(matrix*line.Begin, matrix*line.End);
		}
	};

}