
#include "stdafx.h"

#include "MatrixRepository.h"

namespace RevolutionSolids
{

	Matrix MatrixRepository::GetIdentityMatrix()
	{
		Matrix matrix;
		matrix.Data[0][0] = matrix.Data[1][1] = matrix.Data[2][2] = matrix.Data[3][3] = 1;
		return matrix;
	}

	Matrix MatrixRepository::GetRotationMatrixOX(double phi)
	{
		Matrix matrix;
		matrix.Data[0][0] = 1;
		matrix.Data[1][1] = cos(phi);
		matrix.Data[2][1] = -sin(phi);
		matrix.Data[1][2] = sin(phi);
		matrix.Data[2][2] = cos(phi);
		matrix.Data[3][3] = 1;
		return matrix;
	}

	Matrix MatrixRepository::GetRotationMatrixOY(double phi)
	{
		Matrix matrix;
		matrix.Data[0][0] = cos(phi);
		matrix.Data[0][2] = sin(phi);
		matrix.Data[1][1] = 1;
		matrix.Data[3][3] = 1;
		matrix.Data[2][0] = -sin(phi);
		matrix.Data[2][2] = cos(phi);
		return matrix;
	}

	Matrix MatrixRepository::GetRotationMatrixOZ(double phi)
	{
		Matrix matrix;
		matrix.Data[0][0] = cos(phi);
		matrix.Data[0][1] = -sin(phi);
		matrix.Data[1][0] = sin(phi);
		matrix.Data[1][1] = cos(phi);
		matrix.Data[2][2] = matrix.Data[3][3] = 1;
		return matrix;
	}

	Matrix MatrixRepository::GetScaleMatrix(double sx, double sy, double sz)
	{
		Matrix matrix;
		matrix.Data[0][0] = sx;
		matrix.Data[1][1] = sy;
		matrix.Data[2][2] = sz;
		return matrix;
	}

	Matrix MatrixRepository::GetTranslationMatrix(double tx, double ty, double tz)
	{
		Matrix matrix = GetIdentityMatrix();
		matrix.Data[0][3] = tx;
		matrix.Data[1][3] = ty;
		matrix.Data[2][3] = tz;
		return matrix;
	}

	Matrix MatrixRepository::GetPerspectiveMatrix(double r,
		double l, double t, double b, double n, double f )
	{
		Matrix matrix;
		matrix.Data[0][0] = 2 * n / (r - l);
		matrix.Data[0][2] = (r + l) / (r - l);
		matrix.Data[1][1] = 2 * n / (t - b);
		matrix.Data[1][2] = (t + b) / (t - b);
		matrix.Data[2][2] = (n + f) / (n - f);
		matrix.Data[2][3] = 2 * n*f / (n - f);
		matrix.Data[3][2] = -1;
		return matrix;
	}
	
}