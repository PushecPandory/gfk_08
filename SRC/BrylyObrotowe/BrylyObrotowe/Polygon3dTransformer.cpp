#include "stdafx.h"


#include "Polygon3dTransformer.h"

namespace RevolutionSolids
{
	Polygon3d Polygon3dTransformer::Transform(const Polygon3d& polygon)
	{

		auto matrix = MatrixRepository::GetTranslationMatrix(0, -polygon.GetMiddleY(), 0);
		auto newPolygon = matrix*polygon;

		matrix = MatrixRepository::GetRotationMatrixOX(_rotationVector.X())
			*MatrixRepository::GetRotationMatrixOY(_rotationVector.Y())
			*MatrixRepository::GetRotationMatrixOZ(_rotationVector.Z());

		newPolygon = matrix*newPolygon;
		
		if (_view == SceneView::Perspective)
		{
			matrix = MatrixRepository::GetTranslationMatrix(0,0,300);
			newPolygon = matrix*newPolygon;
			matrix = MatrixRepository::GetPerspectiveMatrix(150, -150, 150, -150, 50, 600);
			//GetPerspectiveMatrix(r,l,t,b,-310,310);
			newPolygon = matrix*newPolygon;
			//auto transMatrix = MatrixRepository::GetTranslationMatrix(1, 1, 0);
			//newPolygon = transMatrix*newPolygon;
		}
		else
		{
			matrix = MatrixRepository::GetScaleMatrix(0.6, 0.6, 0.6);
				newPolygon = matrix*newPolygon;

				matrix = MatrixRepository::GetTranslationMatrix(150, 150, 0);
				newPolygon = matrix*newPolygon;
		}
		
		return newPolygon;
	}
}

