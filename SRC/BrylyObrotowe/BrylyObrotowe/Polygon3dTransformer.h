#pragma once

#include "Matrix.h"
#include "Polygon3d.h"
#include "MatrixRepository.h"
#include "SceneView.h"
namespace RevolutionSolids
{
	class Polygon3dTransformer
	{

	public:
		Polygon3dTransformer(): _rotationVector(Vector(0,0,0)), _translationVector(Vector(0,0,0)), _scale(Vector(1,1,1)), _view(SceneView::Orthographic) {}
		void SetRotation(const Vector& rotationVector)
		{
			_rotationVector = rotationVector;
		}

		void SetTranslation(const Vector& translationVector)
		{
			_translationVector = translationVector;
		}

		void SetScale(const Vector& scaleVector)
		{
			_scale = scaleVector;
		}
		void SetView(SceneView view)
		{
			_view = view;
		}

		Polygon3d Transform(const Polygon3d& polygon);

	private:
		Vector _rotationVector, _translationVector, _scale;
		SceneView _view;
	};
}