
#include "Polygon3dBuilderTests.h"

namespace RevolutionSolidsTests
{
	void Polygon3dBuilderTests::Run()
	{
		TestDistances();
	}

	void Polygon3dBuilderTests::TestDistances()
	{
		RevolutionSolids::PolygonBuilder2d polygonBuilder2d;

		polygonBuilder2d.AddPoint(3, 3);
		polygonBuilder2d.AddPoint(3, 5);
		polygonBuilder2d.AddPoint(0, 5);
		polygonBuilder2d.AddPoint(0, 3);

		RevolutionSolids::Polygon3dBuilder polygon3dBuilder;
		

		if (!Validate(polygon3dBuilder.Rotate(polygonBuilder2d.Build2dPolygon())))
			throw AssertException("polygon built is not valid");


	}

	bool Validate(const RevolutionSolids::Polygon3d& polygon)
	{
		
	}
}
