#pragma once

#include <vector>
#include "Vector.h"
#include "Polygon2d.h"
#include "Rectangle.h"
#include "Polygon3d.h"
namespace RevolutionSolids
{
	//rotates figure from 2d to 3d around 0Z axis
	class Polygon3dBuilder
	{
	private:
		const int EdgesCount = 18;

	public:
		Polygon3d Rotate(const Polygon2d& polygon);
	};
}