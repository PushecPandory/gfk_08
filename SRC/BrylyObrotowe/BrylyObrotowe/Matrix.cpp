#include "stdafx.h"


#include "Matrix.h"

	Matrix::Matrix()
	{
		Data[0][0] = 0.0; Data[0][1] = 0.0; Data[0][2] = 0.0; Data[0][3] = 0.0;
		Data[1][0] = 0.0; Data[1][1] = 0.0; Data[1][2] = 0.0; Data[1][3] = 0.0;
		Data[2][0] = 0.0; Data[2][1] = 0.0; Data[2][2] = 0.0; Data[2][3] = 0.0;
		Data[3][0] = 0.0; Data[3][1] = 0.0; Data[3][2] = 0.0; Data[3][3] = 1.0;
	}

	Matrix Matrix::operator* (const Matrix& matrix)
	{
		int i, j, k;
		Matrix result;

		for (i = 0; i < 4; i++)
			for (j = 0; j < 4; j++)
			{
			result.Data[i][j] = 0.0;
			for (k = 0; k < 4; k++)
				result.Data[i][j] = result.Data[i][j] + (Data[i][k] * matrix.Data[k][j]);
			}
		return result;
	}

	Vector operator* (const Matrix& matrix, const Vector& vector)
	{
		unsigned int i, j;
		Vector result;

		for (i = 0; i < 4; i++)
		{
			result.Data[i] = 0.0;
			for (j = 0; j < 4; j++) result.Data[i] = result.Data[i] + (matrix.Data[i][j] * vector.Data[j]);
		}
		return result;
	}
