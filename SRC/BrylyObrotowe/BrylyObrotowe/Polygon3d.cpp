#include "stdafx.h"
#include <iostream>
#include "Polygon3d.h"

namespace RevolutionSolids
{
	double min(double a, double b)
	{
		return a > b ? b : a;
	}
	double min(double a, double b, double c, double d)
	{
		return min(min(a, b), min(c, d));
	}

	double max(double a, double b)
	{
		return a < b ? b : a;
	}
	double max(double a, double b, double c, double d)
	{
		return max(max(a, b), max(c, d));
	}


	Polygon3d operator*(const Matrix& matrix, const Polygon3d& polygon)
	{
		double maxY, minY;
		double maxZ, minZ;
		std::vector<Rectangle> newFigure;
		for each (Rectangle rect in polygon._figure)
		{
			auto newRect = matrix*rect;
			newFigure.push_back(newRect);
			minY = min(newRect._points[0].Y(), newRect._points[1].Y(), newRect._points[2].Y(),
				newRect._points[3].Y());
			maxY = max(newRect._points[0].Y(), newRect._points[1].Y(), newRect._points[2].Y(),
				newRect._points[3].Y());

			minZ = min(newRect._points[0].Z(), newRect._points[1].Z(), newRect._points[2].Z(),
				newRect._points[3].Z());
			maxZ = max(newRect._points[0].Z(), newRect._points[1].Z(), newRect._points[2].Z(),
				newRect._points[3].Z());

		}
		return Polygon3d(newFigure, (maxY-minY)/2, (maxZ-minZ)/2);
	}

	void Polygon3d::Draw(wxDC& dc, bool transparency)
	{
		for each (Rectangle rect in _figure)
		{
			rect.Draw(dc, transparency);
		}
	}

}