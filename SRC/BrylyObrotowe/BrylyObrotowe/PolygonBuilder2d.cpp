#include "stdafx.h"


#include "PolygonBuilder2d.h"

namespace RevolutionSolids
{

	void PolygonBuilder2d::AddPoint(double x, double y)
	{
		if (_last.X() >= 0)
		{
			_lines.push_back(Line(_last, Vector(x, y, 0)));
		}
		_last = Vector(x, y, 0);
	}

	Polygon2d PolygonBuilder2d::Build2dPolygon()
	{
		return Polygon2d(_lines);
	}
}