#include "stdafx.h"
///-----------------------------------------------------------------
///
/// @file      BrylyObrotoweFrm.cpp
/// @author    Wiktor
/// Created:   2015-01-29 13:28:14
/// @section   DESCRIPTION
///            BrylyObrotoweFrm class implementation
///
///------------------------------------------------------------------

#include "BrylyObrotoweFrm.h"
#include <wx/dcbuffer.h>
#include <wx/spinctrl.h>
#include <wx/filedlg.h>
//Do not add custom headers between
//Header Include Start and Header Include End
//wxDev-C++ designer will remove them
////Header Include Start
////Header Include End

//----------------------------------------------------------------------------
// BrylyObrotoweFrm
//----------------------------------------------------------------------------
//Add Custom Events only in the appropriate block.
//Code added in other places will be removed by wxDev-C++
////Event Table Start
BEGIN_EVENT_TABLE(BrylyObrotoweFrm,wxFrame)
	////Manual Code Start
	////Manual Code End
	
	EVT_CLOSE(BrylyObrotoweFrm::OnClose)
	EVT_LEFT_DOWN(BrylyObrotoweFrm::BrylyObrotoweFrmLeftDown0)
	EVT_CHECKBOX(ID_WXCHECKBOX1, BrylyObrotoweFrm::WxCheckBox1Click)
	EVT_BUTTON(ID_WXBUTTON4,BrylyObrotoweFrm::WxButton4Click)
	EVT_BUTTON(ID_WXBUTTON3,BrylyObrotoweFrm::WxButton3Click)
	EVT_BUTTON(ID_WXBUTTON2,BrylyObrotoweFrm::WxButton2Click)
	EVT_BUTTON(ID_WXBUTTON1,BrylyObrotoweFrm::WxButton1Click)
	
	EVT_COMMAND_SCROLL(ID_WXSCROLLBAR3,BrylyObrotoweFrm::WxScrollBar3Scroll)
	EVT_COMMAND_SCROLL_THUMBTRACK(ID_WXSCROLLBAR3,BrylyObrotoweFrm::WxScrollBar3Scroll)
	
	EVT_COMMAND_SCROLL(ID_WXSCROLLBAR2,BrylyObrotoweFrm::WxScrollBar2Scroll)
	EVT_COMMAND_SCROLL_THUMBTRACK(ID_WXSCROLLBAR2,BrylyObrotoweFrm::WxScrollBar2Scroll)
	
	EVT_COMMAND_SCROLL(ID_WXSCROLLBAR1,BrylyObrotoweFrm::WxScrollBar1Scroll)
	EVT_COMMAND_SCROLL_THUMBTRACK(ID_WXSCROLLBAR1,BrylyObrotoweFrm::WxScrollBar1Scroll)
	
	EVT_UPDATE_UI(ID_WXPANEL2,BrylyObrotoweFrm::WxPanel2UpdateUI)
	
	EVT_UPDATE_UI(ID_WXPANEL1,BrylyObrotoweFrm::WxPanel1UpdateUI)
END_EVENT_TABLE()
////Event Table End

BrylyObrotoweFrm::BrylyObrotoweFrm(wxWindow *parent, wxWindowID id, const wxString &title, const wxPoint &position, const wxSize& size, long style)
: wxFrame(parent, id, title, position, size, style), _model(RevolutionSolids::Polygon3dBuilder(), RevolutionSolids::Polygon3dTransformer())
{
	CreateGUIControls();
}

BrylyObrotoweFrm::~BrylyObrotoweFrm()
{
}

void BrylyObrotoweFrm::CreateGUIControls()
{
	//Do not add custom code between
	//GUI Items Creation Start and GUI Items Creation End
	//wxDev-C++ designer will remove them.
	//Add the custom code before or after the blocks
	////GUI Items Creation Start
	wxInitAllImageHandlers();
	WxPanel1 = new wxPanel(this, ID_WXPANEL1, wxPoint(10, 10), wxSize(300, 300));
	WxPanel2 = new wxPanel(this, ID_WXPANEL2, wxPoint(350, 10), wxSize(300, 300));
	
	WxST_leftPanel = new wxStaticText(this, ID_WXST_LEFTPANEL, _("Tutaj rysujemy linie"), wxPoint(120, 320), wxDefaultSize, 0, _("WxST_leftPanel"));
    WxST_rightPanel = new wxStaticText(this, ID_WXST_RIGHTPANEL, _("Tu sie wygeneruje 3D"), wxPoint(420, 320), wxDefaultSize, 0, _("WxST_rightPanel"));
    WxST_coordinatesPanel = new wxStaticText(this, ID_WXST_COORDINATESPANEL, _(" "), wxPoint(120, 350), wxDefaultSize, 0, _("WxST_coordinatesPanel"));

	WxScrollBar1 = new wxScrollBar(this, ID_WXSCROLLBAR1, wxPoint(720, 40), wxSize(240, 30), wxSB_HORIZONTAL, wxDefaultValidator, _("WxScrollBar1"));
	WxScrollBar1->Enable(false);

	WxScrollBar2 = new wxScrollBar(this, ID_WXSCROLLBAR2, wxPoint(720, 120), wxSize(240, 30), wxSB_HORIZONTAL, wxDefaultValidator, _("WxScrollBar2"));
	WxScrollBar2->Enable(false);

	WxScrollBar3 = new wxScrollBar(this, ID_WXSCROLLBAR3, wxPoint(720, 200), wxSize(240, 30), wxSB_HORIZONTAL, wxDefaultValidator, _("WxScrollBar3"));
	WxScrollBar3->Enable(false);
	
	WxST_X = new wxStaticText(this, ID_WXST_X, _("Obrot wokol osi X: 0"), wxPoint(720, 20), wxDefaultSize, 0, _("WxST_X"));
	WxST_Y = new wxStaticText(this, ID_WXST_Y, _("Obrot wokol osi Y: 0"), wxPoint(720, 100), wxDefaultSize, 0, _("WxST_Y"));
	WxST_Z = new wxStaticText(this, ID_WXST_Z, _("Obrot wokol osi Z: 0"), wxPoint(720, 180), wxDefaultSize, 0, _("WxST_Z"));


	WxButton1 = new wxButton(this, ID_WXBUTTON1, _("RYSUJ"), wxPoint(680, 270), wxSize(75, 25), 0, wxDefaultValidator, _("ZROB 3D"));
	WxButton2 = new wxButton(this, ID_WXBUTTON2, _("WYCZYSC"), wxPoint(770, 270), wxSize(75, 25), 0, wxDefaultValidator, _("WYCZYSC"));
	WxButton3 = new wxButton(this, ID_WXBUTTON3, _("ZAPISZ"), wxPoint(860, 270), wxSize(75, 25), 0, wxDefaultValidator, _("ZAPISZ"));
	WxButton4 = new wxButton(this, ID_WXBUTTON4, _("Dodaj punkt"), wxPoint(771, 302), wxSize(75, 25), 0, wxDefaultValidator, _("WxButton4"));

	WxSpinCtrl1 = new wxSpinCtrl(this, ID_WXSPINCTRL1, _("0"), wxPoint(715, 338), wxSize(80, 24), wxSP_ARROW_KEYS, 0, 300, 0);

	WxSpinCtrl2 = new wxSpinCtrl(this, ID_WXSPINCTRL2, _("0"), wxPoint(850, 338), wxSize(80, 24), wxSP_ARROW_KEYS, 0, 300, 0);
	WxCheckBox1 = new wxCheckBox(this, ID_WXCHECKBOX1, _("Rzut perspektywiczny"), wxPoint(770, 368), wxSize(144, 17), 0, wxDefaultValidator, _("WxCheckBox1"));

	SetTitle(_("BrylyObrotowe"));
	SetIcon(wxNullIcon);
	SetSize(8,-2,998,421);
	Center();
	
	////GUI Items Creation End
	WxScrollBar1->SetScrollbar(0, 1, 361, 1,true);
	WxScrollBar1->Enable(true);
	WxScrollBar2->SetScrollbar(0, 1, 361, 1,true);
	WxScrollBar2->Enable(true);
	WxScrollBar3->SetScrollbar(0, 1, 361, 1,true);
	WxScrollBar3->Enable(true);

}

void BrylyObrotoweFrm::OnClose(wxCloseEvent& event)
{
	Destroy();
}

/*
 * WxPanel1UpdateUI
 */
void BrylyObrotoweFrm::WxPanel1UpdateUI(wxUpdateUIEvent& event)
{
    /*
    int x_out, y_out; // wspolrzedne klikniecia na panel_1
    //while(end==0)
	   if(ismouseclick(WM_LBUTTONUP)) {
            int x_m, y_m;
            getmouseclick(WM_LBUTTONUP, x_m, y_m);
            if ((x_m > 10 && x_m < 310) && (y_m > 10 && y_m < 310)) { // 10 i 310 bo nasz panel_1 ma 300x300 i zaczyna sie w 10,10
                x_out = x_m;
                y_out = y_m;
                
                wxString s;
	            s << "Wspolrzedne kliniecia: ";
	            s << 3 + ", " + 3 ;
	            WxST_coordinatesPanel->SetLabel(s);
            }
        }
    //}
	//*/
	
}

/*
 * WxPanel2UpdateUI
 */
void BrylyObrotoweFrm::WxPanel2UpdateUI(wxUpdateUIEvent& event)
{
	// insert your code here
}

/*
 * WxScrollBar1Scroll
 */
void BrylyObrotoweFrm::WxScrollBar1Scroll(wxScrollEvent& event)
{
	wxString s;
	s << "Obrot wokol osi X: ";
	s << WxScrollBar1->GetThumbPosition();
	WxST_X->SetLabel(s);
	_model.SetRotationX(WxScrollBar1->GetThumbPosition()*3.141/180);
	RefreshAndDraw3d();
}

/*
 * WxScrollBar2Scroll
 */
void BrylyObrotoweFrm::WxScrollBar2Scroll(wxScrollEvent& event)
{
	wxString s;
	s << "Obrot wokol osi Y: ";
	s << WxScrollBar2->GetThumbPosition();
	WxST_Y->SetLabel(s);
	_model.SetRotationY(WxScrollBar2->GetThumbPosition()*3.141 / 180);
	RefreshAndDraw3d();
}

/*
 * WxScrollBar3Scroll
 */
void BrylyObrotoweFrm::WxScrollBar3Scroll(wxScrollEvent& event)
{
	wxString s;
	s << "Obrot wokol osi Z: ";
	s << WxScrollBar3->GetThumbPosition();
	WxST_Z->SetLabel(s);
	_model.SetRotationZ(WxScrollBar3->GetThumbPosition()*3.141 / 180);
	RefreshAndDraw3d();
}

/*
 * WxButton1Click
 */
void BrylyObrotoweFrm::WxButton1Click(wxCommandEvent& event)
{
	RefreshAndDraw3d();
	// insert your code here
}

/*
 * WxButton2Click
 */
void BrylyObrotoweFrm::WxButton2Click(wxCommandEvent& event)
{
	_builder = RevolutionSolids::PolygonBuilder2d();
	_polygon2d = _builder.Build2dPolygon();
	_model.Set2dPolygon(_polygon2d);
	RefreshAndDraw2d();
	RefreshAndDraw3d();
}

/*
 * WxButton3Click
 */
void BrylyObrotoweFrm::WxButton3Click(wxCommandEvent& event)
{
	wxFileDialog fd(this, _("Choose a file"), _(""), _(""), _("*.png"), wxFD_SAVE);
	wxScreenDC dcScreen;


	wxBitmap screenshot(wxCoord(300),wxCoord(300), -1);


	wxMemoryDC memDC;

	memDC.SelectObject(screenshot);

	memDC.Blit(wxPoint(0, 0), wxSize(300, 300), &dcScreen, WxPanel2->GetScreenPosition());

	memDC.SelectObject(wxNullBitmap);
	fd.ShowModal();


	screenshot.SaveFile(fd.GetPath(), wxBITMAP_TYPE_PNG);

}

/*
 * WxButton4Click
 */
void BrylyObrotoweFrm::WxButton4Click(wxCommandEvent& event)
{
	RefreshAndDraw2d();
	            /*wxString s;
	            s << "Wspolrzedne kliniecia: ";
	            s << 3 << ", " << 4 ;
	            WxST_coordinatesPanel->SetLabel(s);*/
	
	
	_builder.AddPoint(WxSpinCtrl1->GetValue(), WxSpinCtrl2->GetValue());
	_polygon2d = _builder.Build2dPolygon();

	RefreshAndDraw2d();


}
void BrylyObrotoweFrm::RefreshAndDraw2d()
{
	wxClientDC dc1(WxPanel1);
	wxBufferedDC dc(&dc1);


	dc.SetBackground(wxBrush(RGB(255, 255, 255)));
	dc.Clear();
	_model.Set2dPolygon(_polygon2d);
	_polygon2d.Draw(dc);
}

void BrylyObrotoweFrm::RefreshAndDraw3d()
{
	wxClientDC dc1(WxPanel2);
	wxBufferedDC dc(&dc1);


	dc.SetBackground(wxBrush(RGB(255, 255, 255)));
	dc.Clear();
	_model.Draw3d(dc);
}



void BrylyObrotoweFrm::BrylyObrotoweFrmLeftDown0(wxMouseEvent& event)
{
	
}

void BrylyObrotoweFrm::WxCheckBox1Click(wxCommandEvent& event)
{
	_model.SetView(WxCheckBox1->IsChecked() ? RevolutionSolids::SceneView::Perspective : 
		RevolutionSolids::SceneView::Orthographic );
	RefreshAndDraw3d();
}
