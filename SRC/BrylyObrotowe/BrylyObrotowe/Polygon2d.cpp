#include "stdafx.h"

#include <iostream>
#include "Polygon2d.h"

namespace RevolutionSolids
{
	Polygon2d::Polygon2d(const std::vector<Line>& lines) : Lines(lines)
	{
		_color = 0;
	}

	Polygon2d operator*(const Matrix& matrix, const Polygon2d& polygon)
	{
		std::vector<Line> newLines;

		for each (Line line in polygon.Lines)
		{
			newLines.push_back(matrix*line);
		}

		return Polygon2d(newLines);
	}

	void Polygon2d::Draw(wxDC& dc) const
	{
		for each (Line line in Lines)
		{
			dc.DrawLine(wxPoint(line.Begin.X(), line.Begin.Y()),
				wxPoint(line.End.X(), line.End.Y()));
			std::cout << line.Begin.X() << " " << line.Begin.Y() << std::endl;
		}
	}

	double Polygon2d::GetMiddleY() const
	{
		double minY = 10000, maxY = -10000;
		for each (Line line in Lines)
		{
			auto y = (line.Begin.Y() + line.End.Y()) / 2;
			minY = y < minY ? y : minY;
			maxY = y > maxY ? y : maxY;
		}
		return (minY + maxY) / 2;
	}
}