#include "stdafx.h"


#include "Vector.h"

	Vector Vector::operator- (const Vector& vector)
	{
		unsigned int i;
		Vector Result;
		for (i = 0; i < 4; i++) Result.Data[i] = Data[i] - vector.Data[i];
		return Result;
	}

	Vector operator* (const Vector &vector, double value)
	{
		unsigned int i;
		Vector Result;
		for (i = 0; i < 4; i++) Result.Data[i] = vector.Data[i] * value;
		return Result;
	}
