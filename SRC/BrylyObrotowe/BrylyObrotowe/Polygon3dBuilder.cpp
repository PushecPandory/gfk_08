
#include "stdafx.h"

#include "Polygon3dBuilder.h"

#include <math.h>
#include "Matrix.h"
#include <iostream>
#include "MatrixRepository.h"

namespace RevolutionSolids
{
	Polygon3d Polygon3dBuilder::Rotate(const Polygon2d& polygon)
	{
		//static Matrix rotationMatrix = MatrixRepository::GetRotationMatrixOY((2* M_PI) / EdgesCount);
		std::vector<Rectangle> rotatedFigure;
		
		for each (Line line in polygon.Lines)
		{	
			auto oldLine = line;
			for (int i = 1; i < EdgesCount; ++i)
			{
				auto rotationMatrix = MatrixRepository::GetRotationMatrixOY(2 * M_PI / EdgesCount*i);
				auto newLine = rotationMatrix*line; //line;
				rotatedFigure.push_back(Rectangle(oldLine.Begin, newLine.Begin, newLine.End, oldLine.End));
				oldLine = newLine;
			}
			rotatedFigure.push_back(Rectangle(oldLine.Begin, line.Begin, line.End, oldLine.End));
		}
		return Polygon3d(rotatedFigure,polygon.GetMiddleY(), 0);
	}

}