#pragma once

#include "Vector.h"
#include "Matrix.h"
#include <wx/dc.h>

namespace RevolutionSolids
{

	class Rectangle
	{
	public:
		Rectangle(const Vector& p1, const Vector& p2, const Vector& p3, const Vector& p4)
		{
			_points[0] = p1;
			_points[1] = p2;
			_points[2] = p3;
			_points[3] = p4;
			_outlineColor = wxColor(255,255,255);
			_fillColor = wxColor(128, 128, 128);

		}
		void Draw(wxDC& drawingContext, bool transparency = true);
		friend Rectangle operator*(const Matrix& matrix, const Rectangle& rect);
	public:
		Vector _points[4];
		wxColor _outlineColor, _fillColor;
	};

}