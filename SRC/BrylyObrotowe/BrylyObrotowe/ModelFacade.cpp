
#include "stdafx.h"

#include "ModelFacade.h"

#include "NotImplementedException.h"

namespace RevolutionSolids
{
	void ModelFacade::RefreshDrawedPolygon()
	{
		_drawedPolygon = _transformer.Transform(_rotator.Rotate(_sourcePolygon));
	}

	void ModelFacade::Set2dPolygon(const Polygon2d& polygon)
	{
		_sourcePolygon = polygon;
		RefreshDrawedPolygon();
	}
	void ModelFacade::SetTranslation(const Vector& vector)
	{
		_transformer.SetTranslation(vector);
		RefreshDrawedPolygon();
	}

	void ModelFacade::SetRotationX(double x)
	{
		_rotationX = x;
		_transformer.SetRotation(Vector(_rotationX, _rotationY, _rotationZ));
		RefreshDrawedPolygon();
	}
	void ModelFacade::SetRotationY(double y)
	{
		_rotationY = y;
		_transformer.SetRotation(Vector(_rotationX, _rotationY, _rotationZ));
		RefreshDrawedPolygon();
	}
	void ModelFacade::SetRotationZ(double z)
	{
		_rotationZ = z;
		_transformer.SetRotation(Vector(_rotationX, _rotationY, _rotationZ));
		RefreshDrawedPolygon();
	}

	void ModelFacade::EnableTransparency()
	{
		_transparency = true;
	}

	void ModelFacade::DisableTransparency()
	{
		_transparency = false;
	}

	void ModelFacade::SetView(SceneView view)
	{
		_transformer.SetView(view);
	}

	void ModelFacade::Draw3d(wxDC& drawingContext)
	{
		RefreshDrawedPolygon();
		_drawedPolygon.Draw(drawingContext, _transparency);
	}

	void ModelFacade::Draw2d(wxDC& drawingContext)
	{
		RefreshDrawedPolygon();
		_sourcePolygon.Draw(drawingContext);
	}

}