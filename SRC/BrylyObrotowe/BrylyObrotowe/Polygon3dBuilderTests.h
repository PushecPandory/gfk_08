#pragma once

#include "Polygon3dBuilder.h"
#include "PolygonBuilder2d.h"
#include "AssertException.h"
namespace RevolutionSolidsTests
{

	class Polygon3dBuilderTests
	{
	public:
		void Run();

	private:
		void TestDistances();
		bool Validate(const RevolutionSolids::Polygon3d& polygon);
	};

}