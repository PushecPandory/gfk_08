#pragma once

#include <exception>
#include <string>
namespace RevolutionSolidsTests
{

	class AssertException
	{
	public:
		AssertException(std::string message) : _message(message) {}
		std::string GetMessage() { return _message; }
	private:
		std::string _message;
	};

}