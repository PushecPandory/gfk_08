#pragma once
#include <wx/dc.h>
#include <vector>
#include "Line.h"
#include "Matrix.h"
namespace RevolutionSolids
{

	class Polygon2d
	{
		
	public:
		Polygon2d() : _color(0) {};
		Polygon2d(const std::vector<Line>& lines);
		friend Polygon2d operator*(const Matrix& matrix, const Polygon2d& polygon);
		std::vector<Line> Lines;
		void Draw(wxDC& dc) const;
		double GetMiddleY() const;

	private:
		int _color;
	};
}