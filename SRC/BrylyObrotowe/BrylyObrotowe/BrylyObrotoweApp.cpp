
#include "stdafx.h"
//---------------------------------------------------------------------------
//
// Name:        BrylyObrotoweApp.cpp
// Author:      Wiktor
// Created:     2015-01-29 13:28:14
// Description: 
//
//---------------------------------------------------------------------------

#include "BrylyObrotoweApp.h"
#include "BrylyObrotoweFrm.h"

IMPLEMENT_APP(BrylyObrotoweFrmApp)

bool BrylyObrotoweFrmApp::OnInit()
{
    BrylyObrotoweFrm* frame = new BrylyObrotoweFrm(NULL);
    SetTopWindow(frame);
    frame->Show();
    return true;
}
 
int BrylyObrotoweFrmApp::OnExit()
{
	return 0;
}
