#pragma once

#include "ModelBase.h"
#include "SceneView.h"
#include "Vector.h"
#include "Polygon2d.h"
#include "Polygon3d.h"
#include "Polygon3dBuilder.h"
#include "Polygon3dTransformer.h"
namespace RevolutionSolids
{

	class ModelFacade
	{
	public:
		ModelFacade(Polygon3dBuilder rotator,  Polygon3dTransformer transformer) 
			: _rotator(rotator), _transformer(transformer), _drawedPolygon(Polygon3d()), _transparency(true),
			_rotationX(0), _rotationY(0), _rotationZ(0){}
		void Set2dPolygon(const Polygon2d& polygon);
		void SetTranslation(const Vector& vector);
		void SetRotationX(double x);
		void SetRotationY(double y);
		void SetRotationZ(double z);
		void EnableTransparency();
		void DisableTransparency();
		void SetView(SceneView view);
		void Draw2d(wxDC& drawingContext);
		void Draw3d(wxDC& drawingContext);

	private:
		double _rotationX, _rotationY, _rotationZ;
		void RefreshDrawedPolygon();
		Polygon2d _sourcePolygon;
		Polygon3d _drawedPolygon;
		bool _transparency;
		Polygon3dBuilder _rotator;
		Polygon3dTransformer _transformer;
	};

}