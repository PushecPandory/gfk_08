#ifndef MODELBASE_H
#define MODELBASE_H



#include "SceneView.h"
#include "Vector.h"
#include "Polygon2d.h"
#include <wx/dc.h>

namespace RevolutionSolids
{

	class ModelBase
	{
	public:
		virtual void Set2dPolygon(const Polygon2d& polygon) = 0;
		virtual void SetTranslation(const Vector& vector)=0;
		virtual void SetRotation(const Vector& vector)=0; // in radians
		virtual void EnableTransparency() = 0;
		virtual void DisableTransparency() = 0;
		virtual void SetView(SceneView view) = 0;
		virtual void Draw2d(wxDC& drawingContext) = 0;
		virtual void Draw3d(wxDC& drawingContext) = 0;
	};
}
#endif