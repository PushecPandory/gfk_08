//---------------------------------------------------------------------------
//
// Name:        BrylyObrotoweApp.h
// Author:      Wiktor
// Created:     2015-01-29 13:28:14
// Description: 
//
//---------------------------------------------------------------------------

#ifndef __BRYLYOBROTOWEFRMApp_h__
#define __BRYLYOBROTOWEFRMApp_h__

#ifdef __BORLANDC__
	#pragma hdrstop
#endif

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

class BrylyObrotoweFrmApp : public wxApp
{
	public:
		bool OnInit();
		int OnExit();
};

#endif
