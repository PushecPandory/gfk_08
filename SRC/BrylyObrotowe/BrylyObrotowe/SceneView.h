#ifndef SCENEVIEW_H
#define SCENEVIEW_H

namespace RevolutionSolids
{

	enum SceneView
	{
		Orthographic, Perspective
	};
}
#endif