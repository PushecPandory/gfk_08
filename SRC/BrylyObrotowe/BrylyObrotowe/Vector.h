#ifndef VECTOR_H
#define VECTOR_H


	class Vector
	{
	public:
		double Data[4];

		Vector operator-(const Vector&);
		friend Vector operator*(const Vector&, double);
		Vector()
		{
			SetData(0, 0, 0);
			Data[3] = 1;
		}
		Vector(double x, double y, double z)
		{
			SetData(x, y, z);
			Data[3] = 1;
		}
		void SetData(double x, double y, double z)
		{
			Data[0] = x;
			Data[1] = y;
			Data[2] = z;
		}
		double X() { return Data[0]; }
		double Y() { return Data[1]; }
		double Z() { return Data[2]; }
	};


#endif